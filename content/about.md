---
title: A propos
subtitle: Bonjour 👋
img_path: images/nto.jpg
layout: page
---

Bienvenue sur mon dépôt git, je m'appelle Nicolas Tournier. Je suis docteur en informatique, consultant développement informatique dans la mise en place des pratiques du mouvement DevOps, formateur et développeur.

Actuellement :

- 🔭 Je travaille sur la nouvelle version de mon site en tant que projet personnel
- 🌱 J'apprends le JavaScript et son écosystème coté frontend

## 👨‍💻 Expérience professionnelle

- Consultant DevOps
- Ingénieur développement web
- Ingénieur R&D sécurisation multimédia
- Enseignant vacataire en IUT

## 👨‍🎓 Parcours scolaire

- Doctorat en informatique, sécurisation multimédia
- Master en informatique, combinatoire, algorithmique, sécurté et administration des réseaux
- License en mathématique

## 📖 Publications scientifiques

Pour plus d'informations, les ressources bibliographiques sont disponibles sur ma page personnelle dans la rubrique [publications](http://www.nicolas-tournier.com/web/publications.php).

### 🗺️ Revue internationale

- Analysis of an EMST-Based Path for 3D Meshes (CAD - 2015)

### 🗺️ Conférences internationales

- 3D Multiresolution Synchronization Scheme Based on Feature Point Selection (SPIE 3DIP - 2012)
- 3D Data Hiding for Enhancement and Indexation on Multimedia Medical Data (BIOSTEC MIAD - 2011)
- Finding Robust Vertices for 3D Synchronization Based on Euclidean Minimum Spanning Tree (SPIE 3DIP - 2011)
- Sensitivity Analysis of Euclidean Minimum Spanning Tree (SPIE 3DIP - 2010)

### 🇫🇷 Conférences nationales

- "Filigrane" numérique pour les maillages 3D (DOCTISS - 2011)
- Etude de la robustesse des ACPM dans le cadre du tatouage 3D (AFIG - 2009)
- Tatouage hiérarchique d'un message hiérarchique en vue de la protection vidéo (CORESA - 2009)
