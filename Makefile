build: clear
	hugo

dev: clear
	hugo serve -D

serve: clear
	hugo serve

clear:
	rm -rf public resources docs